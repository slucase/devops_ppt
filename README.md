# DevOps and Docker
This repository provides an example of a CI/CD workflow for a simple web application. This app workflow uses the following technologies:

* Gitlab: used as both the repository and CI tool.
* NodeJS + ReactJS: just got the full application at https://github.com/v4iv/pokedex.
* Docker: used to create a containerized development environment (which in my opinion is neat) and app.
* Kubernetes: used to deploy the application to a production environment.

> **Notice:**
> This repository was initially built in a private repository, with the kubernetes integration pre configured, therefore, its auto deployment features are currently not working but it stills serve as a good example/guide. 

## Done and TODO

- [x] Get a open source web application code to use as an example
- [x] Create basic CI/CD workflow using Gitlab CI
- [x] Create a Dockerfile for  the containerized application
- [x] Create a Dockerfile for  the containerized development environment
- [x] Provide containerized development environment for VS Code ([Remote Development](https://code.visualstudio.com/docs/remote/remote-overview) extension required)
- [ ] Use a local cluster for development (Probably minikube)
- [ ] Add instructions to install and use a local runner (Preferably whithin the cluster)
- [ ] Add instructions, descriptions and explanations as comments in the main files (.gitlab-ci.yml, Dockerfile(s) etc)
- [ ] Update slides and remove possibly copyrighted content
- [ ] Translate slides to English